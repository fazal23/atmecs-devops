FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
COPY . /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
CMD python manage.py test